<?php


use Illuminate\Support\Facades\DB;
use App\Kecamatan;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
	    ['name'=>'Bogor Utara'],
	    ['name'=>'Bogor Selatan'],
	    ['name'=>'Bogor Barat'],
	    ['name'=>'Bogor Timur'],
	    ['name'=>'Tanah Sareal']
	];

		
		DB::table('kecamatan')->insert($data); // Query Builder approach

	    }
}
