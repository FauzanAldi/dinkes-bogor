<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Layanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->default('1');
            $table->integer('kecamatan_id')->default('1');
            $table->string('name');
            $table->text('lat');
            $table->text('long');
            $table->text('lokasi');
            $table->text('email');
            $table->string('picture')->nullable();
            $table->text('link');
            $table->string('telepon');
            $table->integer('created_by')->default('1');
            $table->integer('updated_by')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan');
    }
}
