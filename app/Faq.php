<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
	/**
	 * Table Name
	 * @var string
	 */
    protected $table = "faq"; 

    /**
	 * Primary Key 
	 * @var string
	 */
    protected $primaryKey = "id"; 

    protected $fillable = ['question','answer','created_by','updated_by'];

}
