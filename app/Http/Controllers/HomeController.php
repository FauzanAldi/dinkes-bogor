<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Slider;
use App\Post;
use App\Gallery;
use App\Category;
use App\Banner;
use App\BannerGermas;
use App\Infografis;
use App\Subscribe;
use App\Agenda;
use App\Kecamatan;
use App\Profiles;
use App\Video;
use Request as Get;
class HomeController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
public function __construct()
{
// $this->middleware('auth');
}
/**
* Show the application home.
*
* @return \Illuminate\Contracts\Support\Renderable
*/
public function index()
{
		$twitterid = explode('/', getSetting('twitter'));
		SEOTools::setTitle(getSetting('web_name'));
		SEOTools::setDescription(getSetting('web_description'));
		SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
		SEOTools::setCanonical(getSetting('web_url'));
		SEOTools::opengraph()->setTitle(getSetting('web_name'));
		SEOTools::opengraph()->setDescription(getSetting('web_description'));
		SEOTools::opengraph()->setUrl(getSetting('web_url'));
		SEOTools::opengraph()->setSiteName(getSetting('web_author'));
		SEOTools::opengraph()->addImage(asset('po-content/uploads/'.getSetting('logo')));
		SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
		SEOTools::twitter()->setTitle(getSetting('web_name'));
		SEOTools::twitter()->setDescription(getSetting('web_description'));
		SEOTools::twitter()->setUrl(getSetting('web_url'));
		SEOTools::twitter()->setImage(asset('po-content/uploads/'.getSetting('logo')));
		SEOTools::jsonLd()->setTitle(getSetting('web_name'));
		SEOTools::jsonLd()->setDescription(getSetting('web_description'));
		SEOTools::jsonLd()->setType('WebPage');
		SEOTools::jsonLd()->setUrl(getSetting('web_url'));
		SEOTools::jsonLd()->setImage(asset('po-content/uploads/'.getSetting('logo')));
		$banner=Banner::all();
		$bannergermas=BannerGermas::all();
		$infografis = Infografis::limit('4')->get();
		$beritadinkes=Post::leftJoin('users', 'users.id', 'posts.created_by')
				->leftJoin('categories', 'categories.id', 'posts.category_id')
				->where('categories.id','1')
				->select('posts.*', 'categories.title as ctitle', 'categories.seotitle as cseotitle', 'users.name')
				->limit('3')
				->get();
		// $beritabogor= Post::leftJoin('users', 'users.id', 'posts.created_by')
		// 						->leftJoin('categories', 'categories.id', 'posts.category_id')
		// 						->where('categories.id','2')
		// 						->select('posts.*', 'categories.title as ctitle', 'categories.seotitle as cseotitle', 'users.name')
		// 						->limit('3')
		// 						->get();
		// $beritapuskesmas= Post::leftJoin('users', 'users.id', 'posts.created_by')
		// 						->leftJoin('categories', 'categories.id', 'posts.category_id')
		// 						->where('categories.id','3')
		// 						->select('posts.*', 'categories.title as ctitle', 'categories.seotitle as cseotitle', 'users.name')
		// 						->limit('3')
		// 						->get();
		$beritabogor=[];
		$beritapuskesmas=[];

		$category_beritabogor=Category::where([['id', '=', '2']])->first();
		$category_beritapuskesmas=Category::where([['id', '=', getSetting('category_id')]])->first();
		$arrContextOptions=array(
					"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
					),
					);
		if ($category_beritabogor!=NULL && $category_beritabogor->api!=NULL) {
			$json= json_decode(file_get_contents($category_beritabogor->api, false, stream_context_create($arrContextOptions)));
			foreach ($json as $b) {
			$array=$b;
			}
			$no=0;
			foreach ($array as $a) {
			$beritabogor[$no]=$a;
			if($no>=2){
			break;
			}
			$no++;
			}
		}


		if ($category_beritapuskesmas!=NULL && $category_beritapuskesmas->api!=NULL) {
			$json= json_decode(file_get_contents($category_beritapuskesmas->api, false, stream_context_create($arrContextOptions)));
			$no=0;
			foreach ($json as $a) {
			$beritapuskesmas[$no]=$a;
			if($no>=2){
			break;
			}
			$no++;
			}
		}
		// dd($beritapuskesmas);
		$slider=Slider::all();
		// dd();
		$agenda=Agenda::where('tanggal','=',date('Y-m-d'))->orderBy('waktu','asc')->get()->toArray();
		$profil=Profiles::first();
		$video=Video::first();
		$kecamatan=kecamatan::all();
		$selected_kecamatan=Kecamatan::where('id',Get::get('kecamatan_id'))->first();
		if ((Get::get('category_id')== 1 | 2) && $selected_kecamatan!=NULL) {
			$selected_category=Get::get('category_id');
			return view(getTheme('home'),compact('banner','bannergermas','infografis','beritadinkes','slider','agenda','kecamatan','profil','video','selected_kecamatan','selected_category','beritabogor','beritapuskesmas'));
		}else{
			return view(getTheme('home'),compact('banner','bannergermas','infografis','beritadinkes','slider','agenda','kecamatan','profil','video','beritabogor','beritapuskesmas'));
		}
		
}
	
	public function error404()
{
		$twitterid = explode('/', getSetting('twitter'));
		SEOTools::setTitle('Not Found - '.getSetting('web_name'));
		SEOTools::setDescription(getSetting('web_description'));
		SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
		SEOTools::setCanonical(getSetting('web_url'));
		SEOTools::opengraph()->setTitle('Not Found - '.getSetting('web_name'));
		SEOTools::opengraph()->setDescription(getSetting('web_description'));
		SEOTools::opengraph()->setUrl(getSetting('web_url'));
		SEOTools::opengraph()->setSiteName(getSetting('web_author'));
		SEOTools::opengraph()->addImage(asset('po-content/uploads/'.getSetting('logo')));
		SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
		SEOTools::twitter()->setTitle('Not Found - '.getSetting('web_name'));
		SEOTools::twitter()->setDescription(getSetting('web_description'));
		SEOTools::twitter()->setUrl(getSetting('web_url'));
		SEOTools::twitter()->setImage(asset('po-content/uploads/'.getSetting('logo')));
		SEOTools::jsonLd()->setTitle('Not Found - '.getSetting('web_name'));
		SEOTools::jsonLd()->setDescription(getSetting('web_description'));
		SEOTools::jsonLd()->setType('WebPage');
		SEOTools::jsonLd()->setUrl(getSetting('web_url'));
		SEOTools::jsonLd()->setImage(asset('po-content/uploads/'.getSetting('logo')));
		
		return response()->view(getTheme('404'), [], 404);
	}
	
	public function subscribe(Request $request)
{
		$this->validate($request,[
			'email' => 'required|string|max:255|email'
		]);
		
		$name = explode('@', $request->email);
		$finalname = ucfirst($name[0]);
		
		$request->request->add([
			'name' => $finalname,
			'created_by' => 1,
			'updated_by' => 1
		]);
		$requestData = $request->all();
		Subscribe::create($requestData);
		
		return redirect('contact')->with('flash_message', __('subscribe.send_notif'));
}
}