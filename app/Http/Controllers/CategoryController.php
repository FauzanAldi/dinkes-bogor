<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Category;
use App\Post;

class CategoryController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application category.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($seotitle)
    {	
    	$arrContextOptions=array(
					    "ssl"=>array(
					        "verify_peer"=>false,
					        "verify_peer_name"=>false,
					    ),
					);  
		$category = Category::where([['seotitle', '=', $seotitle],['active', '=', 'Y']])->first();
		
		if($category) {
			$twitterid = explode('/', getSetting('twitter'));
			SEOTools::setTitle($category->title.' - '.getSetting('web_name'));
			SEOTools::setDescription($category->title.' - '.getSetting('web_description'));
			SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
			SEOTools::setCanonical(getSetting('web_url'));
			SEOTools::opengraph()->setTitle($category->title.' - '.getSetting('web_name'));
			SEOTools::opengraph()->setDescription($category->title.' - '.getSetting('web_description'));
			SEOTools::opengraph()->setUrl(getSetting('web_url'));
			SEOTools::opengraph()->setSiteName(getSetting('web_author'));
			SEOTools::opengraph()->addImage($category->picture == '' ? asset('po-content/uploads/'.getSetting('logo')) : getPicture($category->picture, null, $category->updated_by));
			SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
			SEOTools::twitter()->setTitle($category->title.' - '.getSetting('web_name'));
			SEOTools::twitter()->setDescription($category->title.' - '.getSetting('web_description'));
			SEOTools::twitter()->setUrl(getSetting('web_url'));
			SEOTools::twitter()->setImage($category->picture == '' ? asset('po-content/uploads/'.getSetting('logo')) : getPicture($category->picture, null, $category->updated_by));
			SEOTools::jsonLd()->setTitle($category->title.' - '.getSetting('web_name'));
			SEOTools::jsonLd()->setDescription($category->title.' - '.getSetting('web_description'));
			SEOTools::jsonLd()->setType('WebPage');
			SEOTools::jsonLd()->setUrl(getSetting('web_url'));
			SEOTools::jsonLd()->setImage($category->picture == '' ? asset('po-content/uploads/'.getSetting('logo')) : getPicture($category->picture, null, $category->updated_by));
			
			$posts = Post::leftJoin('users', 'users.id', 'posts.created_by')
				->leftJoin('categories', 'categories.id', 'posts.category_id')
				->where([['posts.category_id', '=', $category->id],['posts.active', '=', 'Y']])
				->select('posts.*', 'categories.title as ctitle', 'categories.seotitle as cseotitle', 'users.name')
				->orderBy('posts.id', 'desc')
				->paginate(9);

			if ($category->api) {
				if(stripos($category->seotitle, 'berita-kota-bogor') !== FALSE){
					$json= json_decode(file_get_contents($category->api, false, stream_context_create($arrContextOptions)));
	                foreach ($json as $b) {
	                	$array=$b;
	                }
	                $data = $this->paginate($array);
	                return view(getTheme('category'), compact('category', 'posts','data'));
				}else{
					$json= json_decode(file_get_contents($category->api, false, stream_context_create($arrContextOptions)));
	                $data = $this->paginate($json);
	                return view(getTheme('category'), compact('category', 'posts','data'));
	            }
			}else{
				return view(getTheme('category'), compact('category', 'posts'));
			}

			
			
			
		} else {
			if($seotitle == 'all') {
				$twitterid = explode('/', getSetting('twitter'));
				SEOTools::setTitle('All Category - '.getSetting('web_name'));
				SEOTools::setDescription('All Category - '.getSetting('web_description'));
				SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
				SEOTools::setCanonical(getSetting('web_url'));
				SEOTools::opengraph()->setTitle('All Category - '.getSetting('web_name'));
				SEOTools::opengraph()->setDescription('All Category - '.getSetting('web_description'));
				SEOTools::opengraph()->setUrl(getSetting('web_url'));
				SEOTools::opengraph()->setSiteName(getSetting('web_author'));
				SEOTools::opengraph()->addImage(asset('po-content/uploads/'.getSetting('logo')));
				SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
				SEOTools::twitter()->setTitle('All Category - '.getSetting('web_name'));
				SEOTools::twitter()->setDescription('All Category - '.getSetting('web_description'));
				SEOTools::twitter()->setUrl(getSetting('web_url'));
				SEOTools::twitter()->setImage(asset('po-content/uploads/'.getSetting('logo')));
				SEOTools::jsonLd()->setTitle('All Category - '.getSetting('web_name'));
				SEOTools::jsonLd()->setDescription('All Category - '.getSetting('web_description'));
				SEOTools::jsonLd()->setType('WebPage');
				SEOTools::jsonLd()->setUrl(getSetting('web_url'));
				SEOTools::jsonLd()->setImage(asset('po-content/uploads/'.getSetting('logo')));
				$all='all';
				$category = Category::where('seotitle','like','%puskesmas%')
					->paginate(27);
				// dd($category);
				return view(getTheme('category'), compact('category','all'));
			} else {
				return redirect('404');
			}
		}
	}

	public function paginate($items, $perPage = 9, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        // dd($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, array('path' => Paginator::resolveCurrentPath()));
    }
}
