<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Profiles;

use Yajra\Datatables\Datatables;
use Vinkla\Hashids\Facades\Hashids;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
		if(Auth::user()->can('update-pages')) {
            $profile = Profiles::first();

            return view('backend.profile.edit', compact('profile'));
        } else {
            return redirect('forbidden');
        }
    }

    public function update(Request $request)
    {
        if(Auth::user()->can('update-pages')) {
            $this->validate($request,[
                'title' => 'required'
            ]);
            $requestData = $request->all();

            $pages = Profiles::first();
            $pages->update($requestData);

            return redirect(route('edit-profile'))->with('flash_message', __('pages.update_notif'));
        } else {
            return redirect('forbidden');
        }
    }

}