<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Layanan;
use App\Kecamatan;
use Yajra\Datatables\Datatables;
use Vinkla\Hashids\Facades\Hashids;

class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
		if(Auth::user()->can('read-layanan')) {
			return view('backend.layanan.datatable');
		} else {
			return redirect('forbidden');
		}
    }
	
	/**
	 * Displays datatables front end view
	 *
	 * @return \Illuminate\View\View
	 */
    public function getIndex()
	{
		if(Auth::user()->can('read-layanan')) {
			return view('backend.layanan.datatable');
		} else {
			return redirect('forbidden');
		}
	}
	
	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function anyData()
	{
		$layanan = Layanan::leftJoin('users', 'users.id', '=', 'layanan.created_by')
			->select('layanan.*', 'users.id as uid', 'users.name as uname');
		return Datatables::of($layanan)
			->addColumn('check', function ($category) {
				$check = '<div style="text-align:center;">
					<input type="checkbox" id="titleCheckdel" />
					<input type="hidden" class="deldata" name="id[]" value="'.Hashids::encode($category->id).'" disabled />
				</div>';
				return $check;
			})
			
			->addColumn('category_id', function ($category) {
				if($category->category_id==1)
					return 'Rumah Sakit';
				else{
					return 'Puskesmas';
				}
			})
            ->addColumn('action', function ($category) {
				$btn = '<div style="text-align:center;"><div class="btn-group">';
				$btn .= '<a href="'.url('dashboard/layanan/'.Hashids::encode($category->id).'').'" class="btn btn-secondary btn-xs btn-icon" title="'.__('general.view').'" data-toggle="tooltip" data-placement="left"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.url('dashboard/layanan/'.Hashids::encode($category->id).'/edit').'" class="btn btn-primary btn-xs btn-icon" title="'.__('general.edit').'" data-toggle="tooltip" data-placement="left"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.url('dashboard/layanan/'.Hashids::encode($category->id).'').'" class="btn btn-danger btn-xs btn-icon" data-delete="" title="'.__('general.delete').'" data-toggle="tooltip" data-placement="left"><i class="fa fa-trash"></i></a>';
				$btn .= '</div></div>';
				return $btn;
            })
			->addColumn('control', function ($category) {
				$check = '<div style="text-align:center;"><a href="javascript:void(0);" class="btn btn-secondary btn-xs btn-icon" data-placement="left"><i class="fa fa-plus"></i></a></div>';
				return $check;
			})
			->escapeColumns([])
			->make(true);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
		if(Auth::user()->can('create-layanan')) {
			
			
			return view('backend.layanan.create',['kecamatan'=>Kecamatan::all()]);
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
		if(Auth::user()->can('create-layanan')) {
			$this->validate($request,[
				'name'=>'required',
				'kecamatan_id' =>'required',
				'picture'=>'required',
				'category_id'=>'required',
				'lat'=>'required',
				'long'=>'required',
				'lokasi'=>'required',
				'email'=>'required',
				'telepon'=>'required'

			]);

			$request->request->add([
				'created_by' => Auth::User()->id,
				'updated_by' => Auth::User()->id
			]);
			$requestData = $request->all();

			Layanan::create($requestData);
			
			return redirect('dashboard/layanan')->with('flash_message', __('Berhasil Membuat data'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
		if(Auth::user()->can('read-layanan')) {
			$ids = Hashids::decode($id);
			$layanan = Layanan::findOrFail($ids[0]);

			return view('backend.layanan.show', compact('layanan'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
		if(Auth::user()->can('update-layanan')) {
			$ids = Hashids::decode($id);
			$layanan = Layanan::findOrFail($ids[0]);
			//$tree = new layanan;
			$kecamatan=Kecamatan::all();
			return view('backend.layanan.edit', compact('layanan','kecamatan'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
		if(Auth::user()->can('update-layanan')) {
			$ids = Hashids::decode($id);
			$this->validate($request,[
				'name'=>'required',
				'picture'=>'required',
				'category_id'=>'required',
				'kecamatan_id' =>'required',
				'lat'=>'required',
				'long'=>'required',
				'lokasi'=>'required',
				'email'=>'required',
				'telepon'=>'required',

			]);
			$request->request->add([
				'updated_by' => Auth::User()->id
			]);
			$requestData = $request->all();

			$layanan = Layanan::findOrFail($ids[0]);
			$layanan->update($requestData);

			return redirect('dashboard/layanan')->with('flash_message', __('Berhasil Memperbaharui Data'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
		if(Auth::user()->can('delete-layanan')) {
			$ids = Hashids::decode($id);
			Layanan::destroy($ids[0]);

			return redirect('dashboard/layanan')->with('flash_message', __('Berhasil Menghapus Data'));
		} else {
			return redirect('forbidden');
		}
    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function deleteAll(Request $request)
    {
		if(Auth::user()->can('delete-layanan')) {
			if ($request->has('id')) {
				$ids = $request->id;
				foreach($ids as $id){
					$idd = Hashids::decode($id);
					Layanan::destroy($idd[0]);
				}
				return redirect('dashboard/layanan')->with('flash_message', __('Berhasil Menghapus Data'));
			} else {
				return redirect('dashboard/layanan')->with('flash_message', __('Belum terdapat data yang di pilih'));
			}
		} else {
			return redirect('forbidden');
		}
    }

    public function kecamatan()
    {
    	return Kecamatan::all();
    }
}
