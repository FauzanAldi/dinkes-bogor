<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Dokumen;

use Yajra\Datatables\Datatables;
use Vinkla\Hashids\Facades\Hashids;

class DokumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
		if(Auth::user()->can('read-dokumen')) {
			return view('backend.dokumen.datatable');
		} else {
			return redirect('forbidden');
		}
    }
	
	/**
	 * Displays datatables front end view
	 *
	 * @return \Illuminate\View\View
	 */
    public function getIndex()
	{
		if(Auth::user()->can('read-dokumen')) {
			return view('backend.dokumen.datatable');
		} else {
			return redirect('forbidden');
		}
	}
	
	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function anyData()
	{
		$dokumen = Dokumen::leftJoin('users', 'users.id', '=', 'dokumen.created_by')
			->select('dokumen.*', 'users.id as uid', 'users.name as uname');
		return Datatables::of($dokumen)
			->addColumn('check', function ($category) {
				$check = '<div style="text-align:center;">
					<input type="checkbox" id="titleCheckdel" />
					<input type="hidden" class="deldata" name="id[]" value="'.Hashids::encode($category->id).'" disabled />
				</div>';
				return $check;
			})
			
            ->addColumn('action', function ($category) {
				$btn = '<div style="text-align:center;"><div class="btn-group">';
				$btn .= '<a href="'.url('dashboard/dokumen/'.Hashids::encode($category->id).'').'" class="btn btn-secondary btn-xs btn-icon" title="'.__('general.view').'" data-toggle="tooltip" data-placement="left"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.url('dashboard/dokumen/'.Hashids::encode($category->id).'/edit').'" class="btn btn-primary btn-xs btn-icon" title="'.__('general.edit').'" data-toggle="tooltip" data-placement="left"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.url('dashboard/dokumen/'.Hashids::encode($category->id).'').'" class="btn btn-danger btn-xs btn-icon" data-delete="" title="'.__('general.delete').'" data-toggle="tooltip" data-placement="left"><i class="fa fa-trash"></i></a>';
				$btn .= '</div></div>';
				return $btn;
            })
			->addColumn('control', function ($category) {
				$check = '<div style="text-align:center;"><a href="javascript:void(0);" class="btn btn-secondary btn-xs btn-icon" data-placement="left"><i class="fa fa-plus"></i></a></div>';
				return $check;
			})
			->escapeColumns([])
			->make(true);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
		if(Auth::user()->can('create-dokumen')) {
			
			
			return view('backend.dokumen.create');
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {	
    	// dd($request->file('file'));
		if(Auth::user()->can('create-dokumen')) {
			$this->validate($request,[
				'judul' => 'required',
				'penulis' => 'required',
				'kategori' => 'required',
				'tahun' => 'required',
				'file' => 'required'
			]);

			$request->request->add([
				'created_by' => Auth::User()->id,
				'updated_by' => Auth::User()->id
			]);
			$requestData = $request->all();

			Dokumen::create($requestData);
			
			return redirect('dashboard/dokumen')->with('flash_message', __('Berhasil Membuat data'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
		if(Auth::user()->can('read-dokumen')) {
			$ids = Hashids::decode($id);
			$dokumen = Dokumen::findOrFail($ids[0]);

			return view('backend.dokumen.show', compact('dokumen'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
		if(Auth::user()->can('update-dokumen')) {
			$ids = Hashids::decode($id);
			$dokumen = Dokumen::findOrFail($ids[0]);
			//$tree = new dokumen;

			return view('backend.dokumen.edit', compact('dokumen'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {	

    	// dd($request->file('file'));
		if(Auth::user()->can('update-dokumen')) {
			$ids = Hashids::decode($id);
			$this->validate($request,[
				'judul' => 'required',
				'penulis' => 'required',
				'kategori' => 'required',
				'tahun' => 'required',
				'file' => 'required'
			]);
			$request->request->add([
				'updated_by' => Auth::User()->id
			]);
			$requestData = $request->all();

			$dokumen = Dokumen::findOrFail($ids[0]);
			$dokumen->update($requestData);

			return redirect('dashboard/dokumen')->with('flash_message', __('Berhasil Memperbaharui Data'));
		} else {
			return redirect('forbidden');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
		if(Auth::user()->can('delete-dokumen')) {
			$ids = Hashids::decode($id);
			Dokumen::destroy($ids[0]);

			return redirect('dashboard/dokumen')->with('flash_message', __('Berhasil Menghapus Data'));
		} else {
			return redirect('forbidden');
		}
    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function deleteAll(Request $request)
    {
		if(Auth::user()->can('delete-dokumen')) {
			if ($request->has('id')) {
				$ids = $request->id;
				foreach($ids as $id){
					$idd = Hashids::decode($id);
					Dokumen::destroy($idd[0]);
				}
				return redirect('dashboard/dokumen')->with('flash_message', __('Berhasil Menghapus Data'));
			} else {
				return redirect('dashboard/dokumen')->with('flash_message', __('Belum terdapat data yang di pilih'));
			}
		} else {
			return redirect('forbidden');
		}
    }
}
