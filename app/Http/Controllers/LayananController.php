<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use Request as Get;
use App\Layanan;
use App\Post;
use App\Kecamatan;
use App\Infografis;

class LayananController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application layanan.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($category_id)
    {	
    	$infografis= Infografis::get();
    	$kecamatan_first=Kecamatan::where('id',Get::get('kecamatan'))->first();
		if (Get::get('kecamatan')!=NULL) {
			$layanan = layanan::where([['category_id', '=', $category_id],['kecamatan_id','=',$kecamatan_first->id]])->paginate(4);
		}else{
			$kecamatan_first=Kecamatan::first();
			$layanan = layanan::where([['category_id', '=', $category_id],['kecamatan_id','=',$kecamatan_first->id]])->paginate(4);
		}

		if ($category_id==1) {
			$category='Rumah Sakit';
		}else{
			$category='Puskesmas';
		}
		if($layanan) {
		$twitterid = explode('/', getSetting('twitter'));
		SEOTools::setTitle(getSetting('web_name'));
		SEOTools::setDescription(getSetting('web_description'));
		SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
		SEOTools::setCanonical(getSetting('web_url'));
		SEOTools::opengraph()->setTitle(getSetting('web_name'));
		SEOTools::opengraph()->setDescription(getSetting('web_description'));
		SEOTools::opengraph()->setUrl(getSetting('web_url'));
		SEOTools::opengraph()->setSiteName(getSetting('web_author'));
		SEOTools::opengraph()->addImage(asset('po-content/uploads/'.getSetting('logo')));
		SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
		SEOTools::twitter()->setTitle(getSetting('web_name'));
		SEOTools::twitter()->setDescription(getSetting('web_description'));
		SEOTools::twitter()->setUrl(getSetting('web_url'));
		SEOTools::twitter()->setImage(asset('po-content/uploads/'.getSetting('logo')));
		SEOTools::jsonLd()->setTitle(getSetting('web_name'));
		SEOTools::jsonLd()->setDescription(getSetting('web_description'));
		SEOTools::jsonLd()->setType('WebPage');
		SEOTools::jsonLd()->setUrl(getSetting('web_url'));
		SEOTools::jsonLd()->setImage(asset('po-content/uploads/'.getSetting('logo')));

			
		$kecamatan=Kecamatan::all();
			
			return view(getTheme('layanan'), compact('layanan','category','kecamatan','infografis'));
		}  else {
				return redirect('404');
		
		}
		
	}

	public function layanan(){
		return Layanan::all();
	}

	public function layanan_detail($category_id,$kecamatan_id){
		return Layanan::where('category_id',$category_id)->where('kecamatan_id',$kecamatan_id)->get();
	}
}
