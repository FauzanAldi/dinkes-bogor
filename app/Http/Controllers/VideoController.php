<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Infografis;
use App\AlbumVideo;
use App\Video;

class VideoController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application album.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($seotitle)
    {
		$album = AlbumVideo::where([['seotitle', '=', $seotitle],['active', '=', 'Y']])->first();
		// dd($album);
		$infografis= Infografis::get();
		if($album) {
			$twitterid = explode('/', getSetting('twitter'));
			SEOTools::setTitle($album->title.' - '.getSetting('web_name'));
			SEOTools::setDescription($album->title.' - '.getSetting('web_description'));
			SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
			SEOTools::setCanonical(getSetting('web_url'));
			SEOTools::opengraph()->setTitle($album->title.' - '.getSetting('web_name'));
			SEOTools::opengraph()->setDescription($album->title.' - '.getSetting('web_description'));
			SEOTools::opengraph()->setUrl(getSetting('web_url'));
			SEOTools::opengraph()->setSiteName(getSetting('web_author'));
			SEOTools::opengraph()->addImage(asset('po-content/uploads/'.getSetting('logo')));
			SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
			SEOTools::twitter()->setTitle($album->title.' - '.getSetting('web_name'));
			SEOTools::twitter()->setDescription($album->title.' - '.getSetting('web_description'));
			SEOTools::twitter()->setUrl(getSetting('web_url'));
			SEOTools::twitter()->setImage(asset('po-content/uploads/'.getSetting('logo')));
			SEOTools::jsonLd()->setTitle($album->title.' - '.getSetting('web_name'));
			SEOTools::jsonLd()->setDescription($album->title.' - '.getSetting('web_description'));
			SEOTools::jsonLd()->setType('WebPage');
			SEOTools::jsonLd()->setUrl(getSetting('web_url'));
			SEOTools::jsonLd()->setImage(asset('po-content/uploads/'.getSetting('logo')));
			
			$videos = video::
				leftJoin('albums_videos', 'albums_videos.id', 'video.album_id')
				->where('video.album_id', '=', $album->id)
				->select('video.*', 'albums_videos.title as atitle')
				->orderBy('video.id', 'desc')
				->paginate(9);
			
			return view(getTheme('video'), compact('album', 'videos','infografis'));
		} else {
			if($seotitle == 'all') {
				$twitterid = explode('/', getSetting('twitter'));
				SEOTools::setTitle('All Category - '.getSetting('web_name'));
				SEOTools::setDescription('All Category - '.getSetting('web_description'));
				SEOTools::metatags()->setKeywords(explode(',', getSetting('web_keyword')));
				SEOTools::setCanonical(getSetting('web_url'));
				SEOTools::opengraph()->setTitle('All Category - '.getSetting('web_name'));
				SEOTools::opengraph()->setDescription('All Category - '.getSetting('web_description'));
				SEOTools::opengraph()->setUrl(getSetting('web_url'));
				SEOTools::opengraph()->setSiteName(getSetting('web_author'));
				SEOTools::opengraph()->addImage(asset('po-content/uploads/'.getSetting('logo')));
				SEOTools::twitter()->setSite('@'.$twitterid[count($twitterid)-1]);
				SEOTools::twitter()->setTitle('All Category - '.getSetting('web_name'));
				SEOTools::twitter()->setDescription('All Category - '.getSetting('web_description'));
				SEOTools::twitter()->setUrl(getSetting('web_url'));
				SEOTools::twitter()->setImage(asset('po-content/uploads/'.getSetting('logo')));
				SEOTools::jsonLd()->setTitle('All Category - '.getSetting('web_name'));
				SEOTools::jsonLd()->setDescription('All Category - '.getSetting('web_description'));
				SEOTools::jsonLd()->setType('WebPage');
				SEOTools::jsonLd()->setUrl(getSetting('web_url'));
				SEOTools::jsonLd()->setImage(asset('po-content/uploads/'.getSetting('logo')));
				
				$videos = AlbumVideo::where('active', '=', 'Y')
					->has('videos')
					->paginate(9);
				
				return view(getTheme('video'), compact('videos','infografis'));
			} else {
				return redirect('404');
			}
		}
	}
}
