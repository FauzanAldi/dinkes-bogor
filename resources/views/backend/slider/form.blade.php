<div class="form-row">
	<div class="form-group col-md-6">
		{!! Form::label('name', __('Judul Slider').' *', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('picture', __('Gambar Slider').' *', ['class' => 'control-label']) !!}
		<div class="input-group">
			{!! Form::text('picture', null, ['class' => $errors->has('picture') ? 'form-control is-invalid' : 'form-control', 'id' => 'input-filemanager', 'required' => 'required']) !!}
			<span class="input-group-append">
				<a href="{{ url('po-content/filemanager/dialog.php?type=1&field_id=input-filemanager&relative_url=1&akey='.env('FM_KEY')) }}" id="filemanager" class="btn btn-secondary"><i class="fa fa-file"></i> {{ __('general.browse') }}</a>
			</span>
		</div>
	</div>
	
</div>
<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('deskripsi', __('Deskripsi Slider'), ['class' => 'control-label']) !!}
		{!! Form::textarea('deskripsi', null, ['class' => $errors->has('deskripsi') ? 'form-control is-invalid' : 'form-control']) !!}
	</div>
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('link', __('Link Selengkapnya').' *', ['class' => 'control-label']) !!}
		{!! Form::text('link', null, ['class' => $errors->has('link') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
	</div>
</div>


