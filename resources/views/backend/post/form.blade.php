<div class="row">
	<div class="col-md-9">
		<div class="form-group">
			{!! Form::label('title', __('post.title').' *', ['class' => 'control-label']) !!}
			{!! Form::text('title', null, ['class' => $errors->has('title') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
			{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
		</div>
		<div class="form-group">
			{!! Form::label('content', __('post.content'), ['class' => 'control-label']) !!}
			{!! Form::textarea('content', null, ['class' => $errors->has('content') ? 'form-control is-invalid ht-300-i' : 'form-control ht-300-i']) !!}
			{!! $errors->first('content', '<p class="help-block">:message</p>') !!}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('seotitle', __('post.seotitle').' *', ['class' => 'control-label']) !!}
			{!! Form::text('seotitle', null, ['class' => $errors->has('seotitle') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
			{!! $errors->first('seotitle', '<p class="help-block">:message</p>') !!}
		</div>
		<div class="form-group">
			{!! Form::label('category_id', __('post.category').' *', ['class' => 'control-label']) !!}
			<select class="select-style form-control" id="category_id" name="category_id">
				@if($formMode == 'edit')
					<option value="{{ $post->category_id }}">{{ __('general.selected') }} {{ $post->category->title }}</option>
				@endif
				@foreach($categorys as $c)
					<option value="{{ $c->id }}">{{ $c->title }}</option>
				@endforeach
				{{-- {{ categoryTreeOption($categorys) }} --}}
			</select>
			{!! $errors->first('parent', '<p class="help-block">:message</p>') !!}
		</div>
		<div class="form-group">
			{!! Form::label('picture', __('post.picture'), ['class' => 'control-label']) !!}
			<div class="input-group">
				{!! Form::text('picture', null, ['class' => $errors->has('picture') ? 'form-control is-invalid' : 'form-control', 'id' => 'input-filemanager']) !!}
				<span class="input-group-append">
					<a href="{{ url('po-content/filemanager/dialog.php?type=1&field_id=input-filemanager&relative_url=1&akey='.env('FM_KEY')) }}" id="filemanager" class="btn btn-secondary"><i class="fa fa-file"></i> {{ __('general.browse') }}</a>
				</span>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('file', __('File Dokumen(PDF)'), ['class' => 'control-label']) !!}
			<div class="input-group">
				{!! Form::text('file', null, ['class' => $errors->has('file') ? 'form-control is-invalid' : 'form-control', 'id' => 'input-filemanager-file']) !!}
				<span class="input-group-append">
					<a href="{{ url('po-content/filemanager/dialog.php?type=2&field_id=input-filemanager-file&relative_url=1&akey='.env('FM_KEY')) }}" id="filemanager-multi" class="btn btn-secondary"><i class="fa fa-file"></i> {{ __('general.browse') }}</a>
				</span>
			</div>
		</div>
	</div>
</div>
