<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('question', __('Pertanyaan').' *', ['class' => 'control-label']) !!}
		{!! Form::textarea('question', null, ['class' => $errors->has('question') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('question', '<p class="help-block">:message</p>') !!}
	</div>
	
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('answer', __('Jawaban').' *', ['class' => 'control-label']) !!}
		{!! Form::textarea('answer', null, ['class' => $errors->has('answer') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('answer', '<p class="help-block">:message</p>') !!}
	</div>
	
</div>


