<div class="form-row">
	<div class="form-group col-md-6">
		{!! Form::label('judul', __('Judul Dokumen').' *', ['class' => 'control-label']) !!}
		{!! Form::text('judul', null, ['class' => $errors->has('judul') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('penulis', __('Penulis Dokumen').' *', ['class' => 'control-label']) !!}
		{!! Form::text('penulis', null, ['class' => $errors->has('penulis') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
	</div>
	
</div>

<div class="form-row">
	<div class="form-group col-md-4">
		{!! Form::label('kategori', __('Kategori Dokumen').' *', ['class' => 'control-label']) !!}
		{!! Form::text('kategori', null, ['class' => $errors->has('kategori') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		<p class="help-block">Contoh: Buku, Peraturan, dan Lain-lain)</p>
		{!! $errors->first('kategori', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-4">
		{!! Form::label('tahun', __('Tahun Pembuatan Dokumen').' *', ['class' => 'control-label']) !!}
		{!! Form::number('tahun', null, ['class' => $errors->has('tahun') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('tahun', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-4">
		{!! Form::label('file', __('File Dokumen').' *', ['class' => 'control-label']) !!}
		<div class="input-group">
			{!! Form::text('file', null, ['class' => $errors->has('file') ? 'form-control is-invalid' : 'form-control', 'id' => 'input-filemanager', 'required' => 'required']) !!}
			<span class="input-group-append">
				<a href="{{ url('po-content/filemanager/dialog.php?type=2&field_id=input-filemanager&relative_url=1&akey='.env('FM_KEY')) }}" id="filemanager" class="btn btn-secondary"><i class="fa fa-file"></i> {{ __('general.browse') }}</a>
			</span>
		</div>
	</div>
</div>


