@extends('layouts.app')
@section('title', __('Lihat Data'))

@section('content')
	<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-20">
		<div>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumb-style1 mg-b-10">
					<li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">{{ __('general.dashboard') }}</a></li>
					<li class="breadcrumb-item"><a href="{{ url('/dashboard/components/table') }}">{{ __('general.components') }}</a></li>
					<li class="breadcrumb-item"><a href="{{ url('/dashboard/dokumen/table') }}">{{ __('Data Dokumen') }}</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{ __('Lihat Data') }}</li>
				</ol>
			</nav>
			<h4 class="mg-b-0 tx-spacing--1">{{ __('Lihat Data') }}</h4>
		</div>
		
		<div><a href="{{ url('dashboard/dokumen/table') }}" class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-t-10"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i> {{ __('general.back') }}</a></div>
	</div>
	
	<div class="card">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th>{{ 'Judul Dokumen' }}</th><td>{{ $dokumen->judul }}</td>
						</tr>
						<tr>
							<th>{{ 'Penulis Dokumen' }}</th><td>{{ $dokumen->penulis }}</td>
						</tr>
						<tr>
							<th>{{ 'Kategori Dokumen' }}</th><td>{{ $dokumen->kategori }}</td>
						</tr>
						<tr>
							<th>{{ 'Tahun Pembuatan  Dokumen' }}</th><td>{{ $dokumen->tahun }}</td>
						</tr>

						<tr>
							<th>{{ __('File') }}</th>
							<td>
								@if($dokumen->file != '')
									<a href="{{ asset('po-content/uploads/'.$dokumen->file) }}" target="_blank">Lihat File</a>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
