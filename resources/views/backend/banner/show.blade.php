@extends('layouts.app')
@section('title', __('Lihat Data'))

@section('content')
	<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-20">
		<div>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumb-style1 mg-b-10">
					<li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">{{ __('general.dashboard') }}</a></li>
					<li class="breadcrumb-item"><a href="{{ url('/dashboard/components/table') }}">{{ __('general.components') }}</a></li>
					<li class="breadcrumb-item"><a href="{{ url('/dashboard/banner/table') }}">{{ __('Data Link Aplikasi') }}</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{ __('Lihat Data') }}</li>
				</ol>
			</nav>
			<h4 class="mg-b-0 tx-spacing--1">{{ __('Lihat Data') }}</h4>
		</div>
		
		<div><a href="{{ url('dashboard/banner/table') }}" class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-t-10"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i> {{ __('general.back') }}</a></div>
	</div>
	
	<div class="card">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th>{{ 'nama Aplikasi' }}</th><td>{{ $Banner->name }}</td>
						</tr>
						<tr>
							<th>{{ __('Link Aplikasi') }}</th><td>{{ $Banner->url }}</td>
						</tr>
						<tr>
							<th>{{ __('Gambar') }}</th>
							<td>
								@if($Banner->picture != '')
									<a href="{{ asset('po-content/uploads/'.$Banner->picture) }}" target="_blank">{{ __('gallery.see_image') }}</a>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
