<div class="form-row">
	<div class="form-group col-md-6">
		{!! Form::label('name', __('Judul Layanan').' *', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('picture', __('Foto').' *', ['class' => 'control-label']) !!}
		<div class="input-group">
			{!! Form::text('picture', null, ['class' => $errors->has('picture') ? 'form-control is-invalid' : 'form-control', 'id' => 'input-filemanager', 'required' => 'required']) !!}
			<span class="input-group-append">
				<a href="{{ url('po-content/filemanager/dialog.php?type=1&field_id=input-filemanager&relative_url=1&akey='.env('FM_KEY')) }}" id="filemanager" class="btn btn-secondary"><i class="fa fa-file"></i> {{ __('general.browse') }}</a>
			</span>
		</div>
	</div>
	
</div>

<div class="form-row">
	<div class="form-group col-md-6">
		{!! Form::label('category_id', __('kategori Layanan').' *', ['class' => 'control-label']) !!}
		<select name="category_id" class="select-album form-control" required>
			<option value="1" @if($formMode == 'edit') selected @endif>Rumah sakit</option>
			<option value="2" @if($formMode == 'edit') selected @endif>Puskesmas</option>

		</select>
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('kecamatan_id', __('Kecamatan').' *', ['class' => 'control-label']) !!}
		<select name="kecamatan_id" class="select-album form-control" required>
			@foreach($kecamatan as $k)
			<option value="{{ $k->id }}" @if($formMode == 'edit') @if($layanan->kecamatan_id==$k->id) selected @endif @endif>{{ $k->name }}</option>
			@endforeach
		
		</select>
	</div>
	
</div>

<div class="form-row">
	<div class="form-group col-md-6">
		{!! Form::label('email', __('Email').' *', ['class' => 'control-label']) !!}
		{!! Form::text('email', null, ['class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('telepon', __('No Telepon').' *', ['class' => 'control-label']) !!}
		{!! Form::text('telepon', null, ['class' => $errors->has('telepon') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('telepon', '<p class="help-block">:message</p>') !!}
	</div>

	
	
</div>

<div class="form-row">
	<div class="form-group col-md-6">
		{!! Form::label('lat', __('Lat').' *', ['class' => 'control-label']) !!}
		{!! Form::text('lat', null, ['class' => $errors->has('lat') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('loongl', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('long', __('Long').' *', ['class' => 'control-label']) !!}
		{!! Form::text('long', null, ['class' => $errors->has('long') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('long', '<p class="help-block">:message</p>') !!}
	</div>

	
	
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('link', __('Link Website'), ['class' => 'control-label']) !!}
		{!! Form::text('link', null, ['class' => $errors->has('link') ? 'form-control is-invalid' : 'form-control']) !!}
		{!! $errors->first('link', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('lokasi', __('Alamat').' *', ['class' => 'control-label']) !!}
		{!! Form::textarea('lokasi', null, ['class' => $errors->has('lokasi') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
	</div>
</div>


