<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('name', __('Nama Agenda').' *', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	</div>

	
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('tanggal', __('Tanggal').' *', ['class' => 'control-label']) !!}
		{!! Form::date('tanggal', null, ['class' => $errors->has('tanggal') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-row">
	
	<div class="form-group col-md-6">
		{!! Form::label('waktu', __('Waktu Mulai').' *', ['class' => 'control-label']) !!}
		{!! Form::time('waktu', null, ['class' => $errors->has('waktu') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('waktu', '<p class="help-block">:message</p>') !!}
	</div>
	<div class="form-group col-md-6">
		{!! Form::label('waktu_selesai', __('Waktu Selesai').' *', ['class' => 'control-label']) !!}
		{!! Form::time('waktu_selesai', null, ['class' => $errors->has('waktu_selesai') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('waktu_selesai', '<p class="help-block">:message</p>') !!}
	</div>
</div>
<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('deskripsi', __('Deskripsi Agenda').' *', ['class' => 'control-label']) !!}
		{!! Form::textarea('deskripsi', null, ['class' => $errors->has('deskripsi') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
	</div>

	
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		{!! Form::label('lokasi', __('Lokasi').' *', ['class' => 'control-label']) !!}
		{!! Form::textarea('lokasi', null, ['class' => $errors->has('lokasi') ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}
		{!! $errors->first('lokasi', '<p class="help-block">:message</p>') !!}
	</div>

	
</div>


