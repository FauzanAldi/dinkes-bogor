@extends(getTheme('layouts.app'))

@section('content')
    @if(!isset($all))

    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">{{ $category->title }}</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">

                <div class="card-wrapper">
                  
                  @if($category->api!=NULL && count($data)>0)
                    {{-- {{ dd($data) }} --}}
                    @foreach($data as $b)
                      {{-- {{ dd($b) }} --}}
                      @if(stripos($category->seotitle, 'berita-kota-bogor') !== FALSE)
                        <div class="card-berita">
                          <div class="image-berita"><img src="{{ $b->gambar }}" alt=""></div>
                          <div class="highlight-berita">
                              <div class="tanggal">{{ $b->tanggal }}</div>
                              <div class="judul-berita"><a href="{{ url('detailpost/'.$category->id.'/'.$b->postid) }}">{{ $b->judul }}</a></div>
                          </div>
                        </div>
                      @else
                        <div class="card-berita">
                          <div class="image-berita"><img src="{{ $b->gambar }}" alt=""></div>
                          <div class="highlight-berita">
                              <div class="tanggal">{{ date('d F Y' , strtotime($b->tgl_post)) }}</div>
                              <div class="judul-berita"><a href="{{ url('detailpost/'.$category->id.'/'.$b->no) }}">{{ $b->judul }}</a></div>
                          </div>
                        </div>
                      @endif
                    @endforeach
                  @endif

                  @foreach($posts as $p)
                    <div class="card-berita">
                        <div class="image-berita"><img src="{{ getPicture($p->picture, 'medium', $p->updated_by) }}" alt=""></div>
                        <div class="highlight-berita">
                            <div class="tanggal">{{ date('d F Y' , strtotime($p->created_at)) }}</div>
                            <div class="judul-berita"><a href="{{ prettyUrl($p) }}">{{ $p->title }}</a></div>
                        </div>
                    </div>
                  @endforeach
                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          @if($category->api!=NULL && count($data)>0)
                          {{ $data->links() }}
                          @else
                          {{ $posts->links() }}
                          @endif
                        </ul>
                      </nav>
                </div>
               
               
            </div>
           <div class="right-side-general">
               @include(getTheme('partials.infografis'))
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
                               
            </div>
       </div>
   </div>

    @else

    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Berita Puskesmas</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">

                <div class="card-wrapper">
                  
                  
                    @foreach($category as $b)
                        <div class="card-berita">
                          {{-- <div class="image-berita"><img src="{{ $b->gambar }}" alt=""></div> --}}
                          <div class="highlight-berita">
                              {{-- <div class="tanggal">{{ $b->tanggal }}</div> --}}
                              <div class="judul-berita"><a href="{{ url('category/'.$b->seotitle) }}">{{ $b->title }}</a></div>
                          </div>
                        </div>
                      
                    @endforeach

                  
                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          {{ $category->links() }}
                        </ul>
                      </nav>
                </div>
               
               
            </div>
           <div class="right-side-general">
               @include(getTheme('partials.infografis'))
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
                               
            </div>
       </div>
   </div>

    @endif
@endsection
