@extends(getTheme('layouts.app'))

@section('content')
	<div class="page-title">&nbsp;</div>
	
	@if(!isset($album))
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">List Album Galeri Kegiatan</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">

                <div class="card-wrapper">
                	@foreach($gallerys as $g)
                    <div class="card-galeri">
                        <div class="image"><img src="{{ getPicture($g->Maingallerys->picture, null, $g->updated_by) }}" alt=""></div>
                        <div class="detail-galeri text-center">
                            {{-- <div class="tanggal">Senin, 18 Mei 2020</div> --}}
                            <div class="judul">{{ $g->title }}</div>
                            <a href="{{ url('/album/'.$g->seotitle) }}" class="btn btn-primary mt-2 btn-sm"><i class="fa fa-search"></i> Lihat Album</a>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          {{ $gallerys->links() }}
                        </ul>
                      </nav>
                </div>
               
               
            </div>
           <div class="right-side-general">
                              @include(getTheme('partials.infografis'))
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
              
            </div>
       </div>
   </div>
	@else
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Detail Album</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">
                <div class="title"><i class="fa fa-camera"></i> {{ $album->title }}</div>
                <div class="card-wrapper">
                	@foreach($gallerys as $g)
                    <div class="card-galeri">
                        <div class="image"><a href="{{ getPicture($g->picture, null, $g->updated_by) }}" data-lightbox="img"><img src="{{ getPicture($g->picture, null, $g->updated_by) }}" alt=""></a></div>
                    </div>
             		@endforeach

                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          {{ $gallerys->links() }}
                        </ul>
                      </nav>
                </div>
               
               
            </div>
            <div class="right-side-general">
               @include(getTheme('partials.infografis'))
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
               
            </div>
       </div>
   </div>
	@endif
@endsection

