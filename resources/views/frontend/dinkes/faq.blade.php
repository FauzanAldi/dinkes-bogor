@extends(getTheme('layouts.app'))

@section('content')

	<div class="detail-head">
        <div class="container">
            <div class="title">Faq</div>
        </div>
    </div>

    <div class="wrapper-detail-content">
       <div class="container">
           <div class="contact-wrapper">
           		<div class="row">
                    <div class="col-lg-12">
                    <!-- Accordion -->
                    <div id="accordionExample" class="accordion shadow">
                
                        <!-- Accordion item 1 -->
                        @foreach ($faq as $item)
                        <div class="card">
                        <div id="heading-{{$item->id}}" class="card-header bg-white shadow-sm border-0">
                            <h6 class="mb-0 font-weight-bold"><a href="#" data-toggle="collapse" data-target="#collapse-{{$item->id}}" aria-expanded="false" aria-controls="collapse-{{ $item->id }}" class="d-block position-relative text-dark text-uppercase collapsible-link py-2">{{ $item->question }}</a></h6>
                        </div>
                        <div id="collapse-{{$item->id}}" aria-labelledby="heading-{{$item->id}}" data-parent="#accordionExample" class="collapse">
                            <div class="card-body p-5">
                            <p class="font-weight-light m-0" style="color: black">{{ $item->answer }}</p>
                            </div>
                        </div>
                        </div>
                        @endforeach
                
                    </div>
                    </div>
                </div>
           </div>
       </div>
    </div>

@endsection
