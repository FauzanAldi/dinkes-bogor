@extends(getTheme('layouts.app'))
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ec20fe9aae8a8001a86c564&product=inline-share-buttons" async="async"></script>
@section('content')
  
  @if(!isset($selected))
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">{{ $post->title }} </div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">
                <div class="wrapper-detail">
                    {{-- <div class="fixed"> --}}
                      @if($post->picture!=NULL)
                        {{-- <div class="col-md-4"> --}}
                          <img src="{{ getPicture($post->picture, null, $post->updated_by) }}" class="img-responsive" alt="{{ $post->title }}" alt="">
                        {{-- </div> --}}
                      @endif
                      {{-- @if($post->picture!=NULL) --}}
                        {{-- <div class="col-md-8"> --}}
                          <div class="tanggal"><i class="fa fa-calendar"></i> {{ date('d F Y' , strtotime($post->created_at)) }}</div>
                          {!! $content !!}
                        {{-- </div> --}}
                      {{-- @endif --}}
                    {{-- </div> --}}
                    <hr>
                    
                    <div class="share-button">
                        <div class="title">Bagikan Artikle Ini :</div>
                        <div class="sharethis-inline-share-buttons"></div>
                    </div>
                    @if($post->file!=NULL && stripos($post->file, '.pdf') !== FALSE)
                      <h3 style="margin-top: 50px;">Lampiran File</h3>
                      <iframe src="{{ getPicture($post->file, null, $post->updated_by) }}" style="width: 100%; height: 500px;"></iframe>
                    @endif
                </div>
            </div>
           <div class="right-side-general">
               
               <div class="berita-sidebar">
                   <div class="title">Berita Lainnya</div>
                   @foreach($other_posts as $op)
                   <div class="item-berita">
                       <div class="image-berita">
                           <img src="{{ getPicture($op->picture, null, $op->updated_by) }}" alt="">
                       </div>
                       <div class="tanggal-berita">{{ date('d F Y' , strtotime($op->created_at)) }}</div>
                       <div class="judul-berita"><a href="#">{{ $op->title }}</a></div>
                   </div>
                   @endforeach
                    <div class="text-center"><a href="{{ url('category/'.$post->cseotitle) }}" class="btn btn-flat bg-birupastel">Selengkapnya</a></div>
               </div>
            </div>
       </div>
   </div>
  @else
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">{{ $selected->judul }} </div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
          @if(stripos($category->seotitle, 'berita-kota-bogor') !== FALSE)
           <div class="left-side-general">
              <div class="tanggal"><i class="fa fa-calendar"></i> {{ $selected->tanggal }}</div>
                <br>
                <img src="{{ $selected->gambar }}" alt="{{ $selected->judul }}"  style="margin-bottom: 10px; width: 100%; height: auto;">
                
                {!! $selected->konten !!}
                <div class="share-button">
                            <div class="title">Bagikan Artikle Ini :</div>
                            <div class="sharethis-inline-share-buttons"></div>
                </div>            
           </div>
           @else
           <div class="left-side-general">
              <div class="tanggal"><i class="fa fa-calendar"></i> {{ date('d F Y' , strtotime($selected->tgl_post)) }}</div>
                <br>
                <img src="{{ $selected->gambar }}" alt="{{ $selected->judul }}"  style="margin-bottom: 10px; width: 100%; height: auto;">
                
                {!! $selected->dinamis_konten !!}
                <div class="share-button">
                            <div class="title">Bagikan Artikle Ini :</div>
                            <div class="sharethis-inline-share-buttons"></div>
                </div>            
           </div>
           @endif
           <div class="right-side-general">
               
               <div class="berita-sidebar">
                   <div class="title">Berita Lainnya</div>
                   @php $no=0; @endphp
                   @if(stripos($category->seotitle, 'berita-kota-bogor') !== FALSE)
                     @foreach($array as $op)
                     <div class="item-berita">
                         <div class="image-berita">
                             <img src="{{ $op->gambar }}" alt="">
                         </div>
                         <div class="tanggal-berita">{{ $op->tanggal  }}</div>
                         <div class="judul-berita"><a href="{{ url('detailpost/'.$category->id.'/'.$op->postid) }}">{{ $op->judul }}</a></div>
                     </div>
                     @if($no>=4) @php break; @endphp @endif
                     @php  $no++; @endphp
                     @endforeach
                   @else
                     @foreach($array as $op)
                     <div class="item-berita">
                         <div class="image-berita">
                             <img src="{{ $op->gambar }}" alt="">
                         </div>
                         <div class="tanggal-berita">{{ date('d F Y' , strtotime($op->tgl_post)) }}</div>
                         <div class="judul-berita"><a href="{{ url('detailpost/'.$category->id.'/'.$op->no) }}">{{ $op->judul }}</a></div>
                     </div>
                     @if($no>=4) @php break; @endphp @endif
                     @php  $no++; @endphp
                     @endforeach
                   @endif
                    <div class="text-center"><a href="{{ url('category/'.$category->seotitle) }}" class="btn btn-flat bg-birupastel">Selengkapnya</a></div>
               </div>
            </div>
            
       </div>
   </div>
	@endif
@endsection
