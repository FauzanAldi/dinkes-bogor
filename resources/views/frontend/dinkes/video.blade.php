@extends(getTheme('layouts.app'))

@section('content')
	<div class="page-title">&nbsp;</div>
	
	@if(!isset($album))
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">List Album Video Kegiatan</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">

                <div class="card-wrapper">
                	@foreach($videos as $g)
                    
                    <div class="card-galeri">
                        {{-- <div class="image"><img src="{{ getPicture($g->Mainvideos->picture, null, $g->updated_by) }}" alt=""></div> --}}
                        <div style="text-align: center; margin-bottom: 50px; margin-top: 10px;" class="judul">{{ $g->title }}</div>
                        <div class="detail-galeri text-center">
                            {{-- <div class="tanggal">Senin, 18 Mei 2020</div> --}}
                            
                            <a href="{{ url('/video/'.$g->seotitle) }}" class="btn btn-primary mt-2 btn-sm"><i class="fa fa-search"></i> Lihat Album</a>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          {{ $videos->links() }}
                        </ul>
                      </nav>
                </div>
               
               
            </div>
           <div class="right-side-general">
               <div class="banner-sidebar owl-carousel owl-theme">
               	@foreach($infografis as $i)
                    <div><img src="{{ getPicture($i->picture, 'original', $i->updated_by) }}" alt="{{ $i->name }}"></div>
                  @endforeach
                  
               </div>
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
            </div>
       </div>
   </div>
	@else
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Detail Album</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">
                <div class="title"><i class="fa fa-camera"></i> {{ $album->title }}</div>
                <div class="card-wrapper">
                	@foreach($videos as $g)
                    <div class="card-galeri">
                        <iframe width="100%" height="244" src="{{ $g->link }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
             		@endforeach

                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          {{ $videos->links() }}
                        </ul>
                      </nav>
                </div>
               
               
            </div>
           <div class="right-side-general">
               <div class="banner-sidebar owl-carousel owl-theme">
                   <div><img src="../assets/images/banner-sidebar-1.jpg" alt=""></div>
                   <div><img src="../assets/images/banner-sidebar-2.jpg" alt=""></div>
               </div>
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
            </div>
       </div>
   </div>
	@endif
@endsection

