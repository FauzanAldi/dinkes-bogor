@extends(getTheme('layouts.app'))

@section('content')
	    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Kritik & Saran</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="contact-wrapper">
            <div class="left-side">
                <div class="contact">
                     <div class="title">Kontak Kami</div>
                     <div class="text-content"><i class="fa fa-phone"></i> {{ getSetting('telephone') }}</div>
                     <div class="text-content"><i class="fa fa-envelope"></i> {{ getSetting('email') }}</div>
                     <div class="text-content"><i class="fa fa-map-marker"></i> {{ getSetting('address') }}</div>
                </div>
            </div>
            <div class="right-side">
                <div class="form-kontak">
                    <div class="title">Kirim Pesan</div>
                    <div>
								@if (Session::has('flash_message'))
									<div class="alert alert-success">{{ Session::get('flash_message') }}</div>
								@endif
								
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
                        <form action="{{ url('contact/send') }}" method="post">
                        	{{ csrf_field() }}
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="inputEmail4">Nama</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                              </div>
                              <div class="form-group col-md-6">
                                <label for="inputPassword4">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputAddress">Subject</label>
                              <input type="text" class="form-control" name="subject"  value="{{ old('subject') }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Pesan</label>
                                <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ old('message') }}</textarea>
                              </div>
                            <div class="form-group">
										{!! NoCaptcha::display() !!}
							</div>
                           
                            <button type="submit" class="btn bg-tosca">Kirim Pesan</button>
                          </form>
                    </div>
                </div>
            </div>
            <div class="full-side">
                <div class="map-embed">
                 <iframe src="{{ getSetting('googlemaps') }}" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
           
           </div>
           
       </div>
   </div>
@endsection
