@extends(getTheme('layouts.app'))

@section('content')
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Agenda Kegiatan Dinkes Kota Bogor</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">

            <div class="wrapper-agenda">
                
                    @foreach($agenda as $a)
                    <div class="item-agenda">

                        <div class="calendar">
                            <img src="{{ asset('po-content/frontend/dinkes/src/assets/images/calendar.png') }}" alt="">
                        </div>
                        <div class="agenda" style="padding: 15px 10px 10px 0px;">
                            <div class="tanggal"><a href="{{ route('detail-agenda',$a->tanggal) }}">{{ date('d F Y' , strtotime($a->tanggal)) }}</a></div>
                            {{-- <div class="nama-agenda"><a href="{{ route('detail-agenda',$a->tanggal) }}">{{ $a->name }}</a></div>
                            <div class="lokasi">{{ $a->lokasi }}</div> --}}
                        </div>
                    </div>
                    @endforeach

                
            </div>
            <div class="text-center">
                <nav aria-label="Page navigation example">
                    {{ $agenda->links() }}
                </nav>
            </div>
               
               
            </div>
           <div class="right-side-general">
               @include(getTheme('partials.infografis'))
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
              
            </div>
       </div>
   </div>

@endsection