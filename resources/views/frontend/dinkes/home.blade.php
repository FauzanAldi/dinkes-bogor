@extends(getTheme('layouts.app'))

@section('content')
    <!--SLider Utama-->
    <div class="slider-utama owl-carousel owl-theme">
        @foreach($slider as $s)
        <div class="slider-item">
            <div class="slider-cover">
                <div class="caption">{{ $s->name }}</div>
                <div class="description">{{ $s->deskripsi  }}</div>
                <a href="{{ $s->link }}" class="btn btn-flat btn-sm bg-tosca mt-3">Selengkapnya</a>
            </div>
            <img style="width: 100%; height: 100%;"  src="{{ getPicture($s->picture, 'original', $s->updated_by) }}" alt="">
        </div>
        @endforeach
    </div>
    <!--Sambutan Kadis-->
    <div class="wrapper">
        <div class="container">
            <div class="wrapper-sambutan" style="border-radius: 10px;">
                <div class="left-side">
                    <div class="foto-kadis">
                        <img src="{{ getPicture($profil->picture, null, $profil->updated_by) }}" alt="">
                    </div>
                    <div class="nama-kadis">{{ $profil->title }}</div>
                </div>
                <div class="right-side" style="min-height: 300px;">
                    <div class="title">Sambutan Kepala Dinas</div>
                    <div class="text-content">
                        {{ \Str::limit(strip_tags($profil->content), 600) }}
                    </div>
                    <a href="{{ url('/profile') }}" class="btn btn-flat bg-orenpastel">Selengkapnya</a>
                </div>
            </div>
        </div>
    </div>
    <!--Section 2-->
    <div class="section-video-agenda">
        <div class="container">
            <div class="left-side">
                <div class="title">{{ $video->title }}</div>
                <div class="video" style="border-top-left-radius: 50px 20px;">
                    <iframe style="border-top-left-radius: 50px 20px;" width="100%" height="244" src="{{ $video->link }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="btn-video"><a href="{{ url('video/all') }}">Video Lainnya</a></div>
            </div>
            <div class="right-side">
                <div class="title">Agenda Kegiatan</div>
                <div class="wrapper-agenda" style="min-height:243px; border-top-right-radius: 50px 20px; margin-bottom: 0; padding-bottom: 0; padding-top: 20;"   >
                    {{-- @foreach($agenda as $a)
                    <div class="item-agenda">
                        <div class="calendar">
                            <img src="{{ asset('po-content/frontend/dinkes/src/assets/images/calendar.png') }}" alt="">
                        </div>
                        <div class="agenda">
                            <div class="tanggal">{{ date('d F Y' , strtotime($a->tanggal)) }}</div>
                            <div class="nama-agenda"><a href="#">{{ $a->name }}</a></div>
                            <div class="lokasi">{{ $a->lokasi }}</div>
                        </div>
                    </div>
                    @endforeach --}}
                    <div class="title"><h4>{{ date('d F Y') }}</h4></div>
                    <div class="agenda-slide owl-carousel owl-theme" style="width: 100%;
                    height: 100%;">
                        @php $no=1 @endphp
                        @foreach(array_chunk($agenda, 4) as $group)
                        <div class="item">
                            @foreach($group as $a)
                            <div class="item-agenda">
                                <div class="calendar">
                                    <img src="{{ asset('po-content/frontend/dinkes/src/assets/images/calendar.png') }}" alt="">
                                </div>
                                <div class="agenda">
                                    <div class="tanggal">{{ date('H:i' , strtotime($a['waktu'])) }} - {{ date('H:i' , strtotime($a['waktu_selesai'])) }} </div>
                                    <div class="nama-agenda"><a>{{ $a['name'] }}</a></div>
                                    <div class="lokasi">{{ $a['lokasi'] }}</div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                    {{-- {{ $agenda->links() }} --}}

                </div>
                <div class="btn-agenda"><a href="{{ url('agenda') }}">Lihat Semua Agenda</a></div>
            </div>
        </div>
    </div>
    <!--Banner Space - Germas-->
    <div class="banner-space-germas">
        <div class="container">
            <div class="logo-germas">
                <img src="{{ asset('po-content/frontend/dinkes/src/assets/images/LogoGermas.png') }}" alt="">
            </div>
            <div class="banner-space owl-carousel owl-theme" style="border-radius: 20px;">
                @foreach($bannergermas as $bg)
                <div><img src="{{ getPicture($bg->picture, 'original', $bg->updated_by) }}" alt="{{ $bg->name }}" style="border-radius: 20px;"></div>
                @endforeach
            </div>
        </div>
    </div>
    <!--Infografis-->
    <div class="title-primary">
        <div class="container">Infografis</div>
    </div>
    <div class="infografis">
        <div class="container">
            @foreach($infografis as $i)
            <div class="item"><img src="{{ getPicture($i->picture, 'original', $i->updated_by) }}" alt="{{ $i->name }}"></div>
            @endforeach
        </div>
    </div>
    <div class="text-center mb-3"><a href="{{ url('infografis') }}" class="btn btn-flat bg-orenpastel">Selengkapnya</a></div>
    <!--Berita Dinkes-->
    <div class="wrapper-beritadinkes">
        <div class="title">Berita Dinkes Kota Bogor</div>
        <div class="container">
            <div id="headline-slider" class="left-side owl-carousel owl-theme">
                @foreach($beritadinkes as $bd)
                <div class="headline">
                    <div class="cover">
                        <div class="judul-headline"><a style="color: white;" href="{{ prettyUrl($bd) }}">{{ $bd->title }}</a></div>
                    </div>
                    <img src="{{ getPicture($bd->picture, 'original', $bd->updated_by) }}" alt="{{ $bd->title }}">
                </div>
                @endforeach
            </div>
            <div class="right-side">
                @foreach($beritadinkes as $bd)
                <div class="list-berita">
                    <a href="{{ prettyUrl($bd) }}">{{ $bd->title }}</a>
                </div>
                @endforeach
            </div>
            
        </div>
        <div class="wrapper-button mb-3"><a href="{{ url('category/berita-dinkes') }}" class="btn btn-flat bg-birupastel">Selengkapnya</a></div>
    </div>
    <!--Peta Sebaran-->
    <div class="title-primary" id="peta-sebaran">
        <div class="container">Peta Sebaran Lokasi Rumah Sakit & Puskesmas di Kota Bogor</div>
    </div>
    <div class="wrapper-petasebaran">
        <div class="container" >
            <div class="maps" id="map" style="border-radius: 10px;">
                {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253658.59016022107!2d106.65179985002013!3d-6.595173959455152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c44a0cd6aab5%3A0x301576d14feb9a0!2sBogor%2C%20Kp.%20Parung%20Jambu%2C%20Bogor%20City%2C%20West%20Java!5e0!3m2!1sen!2sid!4v1589727999781!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> --}}
            </div>
            <div class="filter" style="border-radius: 10px;">
                <div class="title">Cari Berdasarkan</div>
                <form action="#peta-sebaran" method="get">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Jenis (Rumah Sakit / Puskesmas)</label>
                        <select name="category_id" class="form-control" id="exampleFormControlSelect1">
                          <option>--Silahkan Pilih Jenis--</option>
                          <option value="1" @if(isset($selected_category)) @if($selected_category==1) selected @endif @endif >Rumah Sakit</option>
                          <option value="2" @if(isset($selected_category)) @if($selected_category==2) selected @endif @endif >Puskesmas</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlSelect2">Wilayah / Kecamatan</label>
                        <select name="kecamatan_id" class="form-control" id="exampleFormControlSelect2">
                        @foreach($kecamatan as $k)
                          <option value="{{ $k->id }}" @if(isset($selected_kecamatan)) @if($selected_kecamatan->id==$k->id) selected @endif @endif>{{ $k->name }}</option>
                        @endforeach
                        </select>
                      </div>
                      <button class="btns w-100 no-border-radius">Cari</button>
                </form>
            </div>
            @if(isset($selected_kecamatan) && isset($selected_category))
            <input type="hidden" id="maps-type" value="detail">
            @else
            <input type="hidden" id="maps-type" value="all">
            @endif
        </div>
    </div>
    <!--Berita Kota & Puskesmas-->
    <div class="wrapper-berita">
        <div class="container">
            <div class="berita-kota">
                <div class="title">Berita Kota Bogor</div>
                @if($beritabogor!=NULL && count($beritabogor)>0)
                    @foreach($beritabogor as $bb)
                    <div class="list-berita">
                        <div class="image"><img src="{{ $bb->gambar }}" alt=""></div>
                        <div class="judul">
                            <div class="tanggal-posting">{{ $bb->tanggal }}</div>
                            <div class="judul-berita"><a href="{{ url('detailpost/2/'.$bb->postid) }}">{{ $bb->judul }}</a></div>
                        </div>
                    </div>
                    @endforeach
                @endif
                <a href="{{ url('category/berita-kota-bogor') }}" class="btn btn-flat bg-orenpastel">Selengkapnya</a>
            </div>
            <div class="berita-puskesmas">
                <div class="title">Berita Puskesmas</div>
                @if($beritapuskesmas!=NULL && count($beritapuskesmas)>0)
                    @foreach($beritapuskesmas as $bp)
                    <div class="list-berita">
                        <div class="image"><img src="{{ $bp->gambar  }}" alt=""></div>
                        <div class="judul">
                            <div class="tanggal-posting">{{ date('d F Y' , strtotime($bp->tgl_post)) }}</div>
                            <div class="judul-berita"><a href="{{ url('detailpost/3/'.$bp->no) }}">{{ $bp->judul }}</a></div>
                        </div>
                    </div>
                    @endforeach
                @endif
                <a href="{{ url('category/all') }}" class="btn btn-flat bg-orenpastel">Selengkapnya</a>
            </div>
        </div>
    </div>
    <!--Twitter & Lokasi -->
    <div class="wrapper-twitlok">
        <div class="container">
            <div class="panel-twitter">
                <div class="title">Terbaru dari Twitter</div>
                <div class="twitter-embed" style="height: 500px;">
                    <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
            <div class="lokasi">
                <div class="title">Lokasi Dinas Kesehatan Kota Bogor</div>
                <div class="lokasi-detail">Jalan Kesehatan No.3, Tanah Sareal, Tanah Sereal, Kota Bogor, Jawa Barat 16161 Telp:(0251) 8331753</div>
                <div class="map-embed" style="height: 500px;">
                    <iframe width="100%" height="500" src="{{ getSetting('googlemaps') }}" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                   {{--  <iframe src="{{ getSetting('googlemaps') }}" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> --}}
                </div>
            </div>
        </div>
    </div>
    <!--Link Banner-->
    <div class="title-primary">
        <div class="container">Link Aplikasi</div>
    </div>
    <div class="wrapper-linkbanner">
        <div id="banner-item-slider" class="container owl-carousel owl-theme">
            @foreach($banner as $b)
            <div class="banner-item"><img src="{{ getPicture($b->picture, 'original', $b->updated_by) }}" alt="{{ $b->name }}"></div>
            @endforeach
        </div>
    </div>
<script>



      var map;

      
      function initMap() {
        map = new google.maps.Map(
            document.getElementById('map'),
            {center: new google.maps.LatLng(-6.597146899999999, 106.8060388), zoom: 12});

        var iconBase =
            '{{ asset('po-content/frontend/icon.jpg') }}';


        
        var type_maps=document.getElementById('maps-type').value;
        if (type_maps=='detail') {
            @php
                if(isset($selected_category) && isset($selected_kecamatan)) { @endphp
                    var url='{{ route('layanan-json-detail',['category_id'=>$selected_category,'kecamatan_id'=>$selected_kecamatan]) }}';
            @php   }
                
            @endphp
        }else{
            var url='{{ route('layanan-json') }}';
        }
        $.getJSON(url, function(json){
            
            //console.log(json[0].lat);
            for (var i = 0; i < json.length; i++) {
                console.log(json[i].lat);
                var marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(json[i].lat, json[i].long), clickable: true});
                var content = json[i].name;
                var infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map,marker);
                    };
                })(marker,content,infowindow));  

            }

        });

        

        
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRelVGbcatJE0olDHWE-QImCAQQqx3h-8&callback=initMap">
    </script>
@endsection
