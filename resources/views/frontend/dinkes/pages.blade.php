@extends(getTheme('layouts.app'))

@section('content')
    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">{{ $pages->title }}</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">
           		{!! $pages->content !!}
              @if($pages->file!=NULL)
                      <h3 style="margin-top: 50px;">Lampiran File</h3>
                      @if(json_decode($pages->file)!=NULL)
                      <ul>
                        @foreach(json_decode($pages->file) as $p) 
                        <li><a href="{{ url('po-content/uploads/'.$p) }}" target="_blank">{{ $p }}</a></li>
                        @endforeach
                      </ul>
                      @else
                      <ul>
                        <li><a href="{{ url('po-content/uploads/'.$pages->file) }}" target="_blank">{{ $pages->file }}</a></li>
                      </ul>
                      @endif
                      
              @endif
            </div>
            @include(getTheme('partials.sidebar'))
       </div>
   </div>
@endsection
