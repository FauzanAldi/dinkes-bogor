@extends(getTheme('layouts.app'))

@section('content')

    <div class="detail-head">
        <div class="container">
            <div class="title">{{ $category }}</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">
           		<form action="" method="get">
                <div class="filter">
                    <div class="text-filter">Tampilkan Berdasarkan Wilayah</div>

                    <div class="text-filter">
                        
                            <select name="kecamatan" class="form-control form-control-sm">
                            	@foreach($kecamatan as $k)
                                	<option value="{{ $k->id }}" @if(Request::get('kecamatan')==$k->id) selected @endif>{{ $k->name }}</option>
                                @endforeach
                            </select>
                        
                    </div>
                    <div class="text-filter"><button class="btn bg-orenpastel">Tampilkan</button></div>
                    

                </div>
                </form>
                <div class="card-wrapper">
                	@if(count($layanan)>0)
                		@foreach($layanan as $l)
		                    <div class="card-jejaring">
		                        <div class="image-jejaring"><img src="{{ getPicture($l->picture, 'original', $l->updated_by) }}" alt=""></div>
		                        <div class="info-jejaring">
		                            <div class="nama-jejaring">{{ $l->name }}</div>
		                            <div class="detail-jejaring"><i class="fa fa-map-marker"></i> {{ $l->lokasi }}</div>
		                            <div class="detail-jejaring"><i class="fa fa-phone"></i> {{ $l->telepon }}</div>
		                            <div class="detail-jejaring"><i class="fa fa-envelope"></i> {{ $l->email }}</div>
		                            <a href="{{ $l->link }}" class="btn btn-flat bg-orenpastel btn-sm mt-2">Kunjungi Website</a>
		                        </div>
		                    </div>
                    	@endforeach
                	@else
                	Maaf belum ada Layanan Kecamatan ini
                	@endif

                    
                   
                    

                </div>
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        {{ $layanan->links() }}
                      </nav>
                </div>
               
               
            </div>
           
           <div class="right-side-general">
               @include(getTheme('partials.infografis'))
               <div class="twitter-embed">
                <a class="twitter-timeline" href="https://twitter.com/BogorDinkes?ref_src=twsrc%5Etfw">Tweets by BogorDinkes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </div>
                              
            </div>
       </div>
   </div>

@endsection