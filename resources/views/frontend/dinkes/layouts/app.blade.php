@php
      use Spatie\Analytics\Period;
 @endphp
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{{ str_replace('_', '-', app()->getLocale()) }}" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="robots" content="index, follow" />
    <meta name="generator" content="{{ config('app.version') }}" />
    <meta name="author" content="{{ getSetting('web_author') }}" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	{!! SEO::generate() !!}
	
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('po-content/uploads/'.getSetting('favicon')) }}" />
	<link rel="stylesheet" href="{{ asset('po-content/frontend/dinkes/node_modules/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('po-content/frontend/dinkes/node_modules/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('po-content/frontend/dinkes/node_modules/animate.css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('po-content/frontend/dinkes/node_modules/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('po-content/frontend/dinkes/node_modules/lightbox2/dist/css/lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('po-content/frontend/dinkes/src/assets/css/main.css') }}">
	
    <style type="text/css">
        body{
              width: 100%;
              height: 1200px;
              display: block;
              position: relative;
              margin: 0 auto;
              z-index: 0;
        }

        body::after {
          background: url({{ getPicture(getSetting('body_background'), 'original', '1') }});
          background-repeat: no-repeat;
          background-size: 100% 100%;
          content: "";
          opacity: 0.2;
          position: fixed;
          top: 0;
          bottom: 0;
          right: 0;
          left: 0;
          z-index: -1;   
        }
    </style>
	@stack('styles')
	
	<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
		]); ?>
	</script>
	
	{!! NoCaptcha::renderJs() !!}
	
	@if(getSetting('google_analytics_id') != '')
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', "{{ getSetting('google_analytics_id') }}"]);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
	@endif
</head>
<body>
      <!--Top Bar-->
    <div class="topbar">
        <div class="social-media">
            <div class="container flex-row">
                <div class="left-side">Media Sosial Kami :</div> 
                <div class="right-side">
                    <div class="icon"><a href="{{ getSetting('facebook') }}"><img src="{{ asset('po-content/frontend/dinkes/src/assets/images/fb.png') }}" alt=""></a></div>
                    <div class="icon"><a href="{{ getSetting('instagram') }}"><img src="{{ asset('po-content/frontend/dinkes/src/assets/images/ig.png') }}" alt=""></a></div>
                    <div class="icon"><a href="{{ getSetting('twitter') }}"><img src="{{ asset('po-content/frontend/dinkes/src/assets/images/twitter.png') }}" alt=""></a></div>
                </div>
            </div>
        </div>
    </div>
    <!--Header-->
    <div class="header">
        <div class="container">
            <div class="left-side">
                <div class="logo"><img src="{{ asset('po-content/uploads/').'/'.getSetting('logo') }}" alt=""></div>
                <div class="logo-title">
                    <div class="top">Dinas Kesehatan</div>
                    <div class="bottom">Kota Bogor</div>
                </div>
            </div>
            <div class="right-side">
                <div class="main-menu">
                   
                    <nav id="menu">
                        <label for="tm" id="toggle-menu">Navigation <span class="drop-icon">▾</span></label>
                        <input type="checkbox" id="tm">
                        <ul class="main-menu clearfix">
                          @each(getTheme('partials.menu'), getMenus(), 'menu', getTheme('partials.menu'))

                        </ul>
                      </nav>
                      
                </div>
            </div>
        </div>
    </div>

    @yield('content')

        <!--Footer-->
    @php
      
      // $fetchRealtimeUsers = $this->fetchRealtimeUsers();
          $analytics = Analytics::getAnalyticsService();
          $results = $analytics->data_realtime
            ->get(
              'ga:228185080',
              'rt:activeUsers'
            );
          
        
    @endphp
    <div class="wrapper-footer" style="height: 30px;">
        <div class="container">
            <div class="col-md-8" style="float: left;">
              <div class="footer-text">Copyright Dinas Kesehatan Kota Bogor 2020 Allright Reserved</div>
            </div>
            <div class="col-md-4" style="float: right;">
              <div class="footer-text">Total Pengunjung Website Saat Ini :  @php if (isset($results[0][0])) {
            echo $results->rows[0][0];
          }else{
            echo 0;
          } @endphp</div>
            </div>
            
        </div>
    </div>
	
	<script src="{{ asset('po-content/frontend/dinkes/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('po-content/frontend/dinkes/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('po-content/frontend/dinkes/node_modules/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('po-content/frontend/dinkes/node_modules/lightbox2/dist/js/lightbox.js') }}"></script>
    <script src="{{ asset('po-content/frontend/dinkes/src/assets/js/main.js') }}"></script>
	
	
	@stack('scripts')
</body>
</html>