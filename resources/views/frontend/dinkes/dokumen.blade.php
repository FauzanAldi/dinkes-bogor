@extends(getTheme('layouts.app'))

{{-- @push('css')



@endpush --}}

@section('content')

	<div class="detail-head">
        <div class="container">
            <div class="title">Dokumen</div>
        </div>
    </div>

    <div class="wrapper-detail-content">
       <div class="container">
           {{-- <div class="contact-wrapper"> --}}
				<table id="myTable">
				    <thead>
				        <tr>
				            <th>No</th>
				            <th>Judul</th>
				            <th>Penulis</th>
				            <th>Kategori</th>
				            <th>Hits</th>
				            <th>File</th>
				            <th>Download File</th>
				        </tr>
				    </thead>
				    <tbody>
				    	@php $no=1; @endphp
				    	@foreach($dokumen as $d)
				        <tr>
				            <td>{{ $no++ }}</td>
				            <td>{{ $d->judul }} <button class="btn btn-danger">{{ $d->tahun }}</button> </td>
				            <td>{{ $d->penulis }}</td>
				            <td>{{ $d->kategori }}</td>
				            <td>{{ $d->hits }}</td>
				            <td><a href="{{ url('po-content/uploads/'.$d->file) }}" class="btn btn-info" target="__blank">Lihat File</a></td>
				            <td>
				            	<form action="{{ route('download_dokumen') }}" method="post">
				            		@csrf
				            		<input type="hidden" name="hits" value="{{ $d->hits+1 }}">
				            		<input type="hidden" name="id" value="{{ $d->id }}">
				            		{{-- <a href="{{ url('po-content/uploads/'.$d->file) }}" class="btn btn-success" download>Download File</a> --}}
				            		<button type="submit" class="btn btn-success">Download File</button>
				            	</form>
				            </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>
           {{-- </div> --}}
       </div>
    </div>


@endsection

@push('scripts')

<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable();
	} );
</script>

@endpush