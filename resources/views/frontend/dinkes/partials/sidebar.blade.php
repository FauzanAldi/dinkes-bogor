           <div class="right-side-general">
               <div class="banner-sidebar owl-carousel owl-theme">
                  @foreach($infografis as $i)
                    <div><img src="{{ getPicture($i->picture, 'original', $i->updated_by) }}" alt="{{ $i->name }}"></div>
                  @endforeach
               </div>
               <div class="berita-sidebar">
                   <div class="title">Berita Dinkes</div>
                   <div class="item-berita">
                       <div class="image-berita">
                           <img src="../assets/images/brt-3.jpg" alt="">
                       </div>
                       <div class="tanggal-berita">Senin, 18 Mei 2020</div>
                       <div class="judul-berita"><a href="#">Test SWAB bagi Orang Dalam Pemantauan (ODP) di Dinkes Kota Bogor</a></div>
                   </div>
                   <div class="item-berita">
                        <div class="image-berita">
                            <img src="../assets/images/brt-4.jpg" alt="">
                        </div>
                        <div class="tanggal-berita">Senin, 18 Mei 2020</div>
                        <div class="judul-berita"><a href="#">Pelaksanaan Tes SWAB di GOR Pajajaran Kota Bogor</a></div>
                    </div>
                    <div class="item-berita">
                        <div class="image-berita">
                            <img src="../assets/images/brt-5.jpg" alt="">
                        </div>
                        <div class="tanggal-berita">Senin, 18 Mei 2020</div>
                        <div class="judul-berita"><a href="#">Pelaksanaan Test SWAB di Stasiun Bogor</a></div>
                    </div>
                    <div class="text-center"><a href="#" class="btn btn-flat bg-birupastel">Selengkapnya</a></div>
               </div>
            </div>