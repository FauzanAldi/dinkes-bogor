@php use App\Infografis;  $infografis=Infografis::all(); @endphp

<div class="banner-sidebar owl-carousel owl-theme">
    @foreach($infografis as $i)
        <div><img src="{{ getPicture($i->picture, 'original', $i->updated_by) }}" alt="{{ $i->name }}"></div>
    @endforeach
</div>