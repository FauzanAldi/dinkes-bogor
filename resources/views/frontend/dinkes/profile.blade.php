@extends(getTheme('layouts.app'))

@section('content')


    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Sambutan Kepala Dinas</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-sambutan">
                <div class="foto-kadis">
                    <img src="{{ getPicture($profil->picture, null, $profil->updated_by) }}" alt="">
                </div>
                <div class="nama-kadis">{{ $profil->title }}</div>
               
            </div>
           <div class="right-side-sambutan">
                <div class="text-content">
                    {!! $profil->content !!}
                </div>
            </div>
       </div>
   </div>

@endsection