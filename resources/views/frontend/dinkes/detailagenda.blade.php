@extends(getTheme('layouts.app'))
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ec20fe9aae8a8001a86c564&product=inline-share-buttons" async="async"></script>
@section('content')

	    <!--Detail Head-->
    <div class="detail-head">
        <div class="container">
            <div class="title">Detail Agenda</div>
        </div>
    </div>
   <!--Wrapper Detail Content-->
   <div class="wrapper-detail-content">
       <div class="container">
           <div class="left-side-general">
                <div class="title text-orenpastel">Tanggal : {{ date('d F Y' , strtotime($agenda->tanggal)) }}</div>
                <table class="table table-striped">
                        <tr>
                          <th>Nama Agenda</th>
                          <th>Waktu Agenda</th>
                          <th>Detail</th>
                        </tr>
                </table>
              <div id="accordion">
                @foreach($list as $l)
                <div class="card">
                  <div class="card-header" id="headingOne">

                    <div class="row">
                      <div class="col-md-4" style="padding-top: 10px;">
                        {{-- <h5 class="mb-0"> --}}
                          
                            {{ $l->name }}
                          {{-- </button> --}}
                        {{-- </h5> --}}
                      </div>
                      <div class="col-md-5">
                        <table class="table table-striped">
                          <tr>
                            <td>{{ date('H:i' , strtotime($l->waktu)) }} - {{ date('H:i' , strtotime($l->waktu_selesai)) }} </td>
                          </tr>
                      </table>
                      </div>
                      <div class="col-md-2" style="text-align: center; padding-top: 10px;">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#{{ str_replace(' ','',$l->name) }}" aria-expanded="false" aria-controls="{{ str_replace(' ','',$l->name) }}">
                          <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>

                  <div id="{{ str_replace(' ','',$l->name) }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    {{-- <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div> --}}
                    <table class="table table-striped">
                        <tr>
                          <th>Lokasi</th>
                          <td>{{ $l->lokasi }} </td> 
                        </tr>
                        <tr>
                          <th>Deskripsi Kegiatan</th>
                          <td>{{ $l->deskripsi }}</td>
                        </tr>
                    </table>
                    
                  </div>
                </div>
                @endforeach
              </div>
                


                
                <div class="text-content">
                    <div class="sub-onsub mb-2">Bagikan Agenda Ini :</div>
                    <div class="sharethis-inline-share-buttons"></div>
                </div>
                
                    
               
            </div>
            @include(getTheme('partials.sidebar'))
       </div>
   </div>



@endsection