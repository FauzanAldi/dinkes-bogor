@extends('layouts.auth')
@section('title', 'Login')

@section('content')
	<div class="content content-fixed content-auth">
		<div class="container">
			<div class="media align-items-stretch justify-content-center ht-100p pos-relative">
				<div class="media-body align-items-center d-none d-lg-flex">
					<div class="mx-wd-600" >
						<img style="border-radius: 10px;" src="{{ asset('po-content/frontend/login.png') }}" class="img-fluid" alt="">
					</div>
				</div>
				
				<div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
					<div class="wd-100p">
						<h3 class="tx-color-01 mg-b-5">{{ __('auth.signin') }}</h3>
						<p class="tx-color-03 tx-16 mg-b-40">{{ __('auth.signin_intro') }}</p>
						<form method="POST" action="{{ route('login') }}">
							@csrf
							<input type="hidden" name="remember" value="">
							<div class="form-group">
								<label>{{ __('auth.email') }}</label>
								<input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('auth.email_text') }}">
								@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
							<div class="form-group">
								<div class="d-flex justify-content-between mg-b-5">
									<label class="mg-b-0-f">{{ __('auth.password') }}</label>
									
								</div>
								<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('auth.password_text') }}">
								@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
							<button class="btn btn-brand-02 btn-block" type="submit">{{ __('auth.signin') }}</button>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
